package javaBeans;

import java.util.*;

public class Etudiant {
	
	private static int nextIDEtudiants = 0;
	
	private int IDetu;
	
	private String nom;
	
	private ArrayList<Note> listNotes;
	
	
	public Etudiant(String nom) {
		this.IDetu = this.nextIDEtudiants++;
		
		this.nom = nom;
		this.listNotes = new ArrayList<Note>();
	}
	
	public double MoyenneEtu() {
		
		return 0.0;
	}


	/**
	 * @return the nextIDEtudiants
	 */
	public static int getNextIDEtudiants() {
		return nextIDEtudiants;
	}


	/**
	 * @param nextIDEtudiants the nextIDEtudiants to set
	 */
	public static void setNextIDEtudiants(int nextIDEtudiants) {
		Etudiant.nextIDEtudiants = nextIDEtudiants;
	}


	/**
	 * @return the iDetu
	 */
	public int getIDetu() {
		return IDetu;
	}


	/**
	 * @param iDetu the iDetu to set
	 */
	public void setIDetu(int iDetu) {
		IDetu = iDetu;
	}


	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}


	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}


	/**
	 * @return the listNotes
	 */
	public ArrayList<Note> getListNotes() {
		return listNotes;
	}


	/**
	 * @param listNotes the listNotes to set
	 */
	public void setListNotes(ArrayList<Note> listNotes) {
		this.listNotes = listNotes;
	}
	
}

