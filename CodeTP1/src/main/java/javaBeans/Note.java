package javaBeans;

public class Note {
	
	private int IDetu;
	
	private int IDmatiere;
	
	private double score;
	
	
	public Note(int etu, int matiere, int note) {
		IDetu = etu;
		IDmatiere = matiere;
		score = note;
	}


	/**
	 * @return the iDetu
	 */
	public int getIDetu() {
		return IDetu;
	}


	/**
	 * @param iDetu the iDetu to set
	 */
	public void setIDetu(int iDetu) {
		IDetu = iDetu;
	}


	/**
	 * @return the iDmatiere
	 */
	public int getIDmatiere() {
		return IDmatiere;
	}


	/**
	 * @param iDmatiere the iDmatiere to set
	 */
	public void setIDmatiere(int iDmatiere) {
		IDmatiere = iDmatiere;
	}


	/**
	 * @return the score
	 */
	public double getScore() {
		return score;
	}


	/**
	 * @param score the score to set
	 */
	public void setScore(double score) {
		this.score = score;
	}
}
