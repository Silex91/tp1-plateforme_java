
package mysite.loginServlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //TODO if needed
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        request.getRequestDispatcher("link.html").include(request, response);

        String name = request.getParameter("name");
        String password = request.getParameter("password");

        if (name.equals("admin") && password.equals("admin123")) {
            out.print("Welcome, <span style='color: blue;'>" + name + "</span>");
            HttpSession session = request.getSession();
            session.setAttribute("name", name);
        } else {
            out.print("<span style='color: red;'>Sorry, username or password error!</span>");
            request.getRequestDispatcher("login.html").include(request, response);
        }
        out.close();
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
