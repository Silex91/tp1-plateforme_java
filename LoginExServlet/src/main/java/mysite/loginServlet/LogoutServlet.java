
package mysite.loginServlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "LogoutServlet", urlPatterns = {"/logout"})
public class LogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html");

	PrintWriter out = response.getWriter();
	//request.getRequestDispatcher("link.html").include(request, response);

	HttpSession session = request.getSession();
	session.invalidate();
	
	
	response.sendRedirect(request.getContextPath() + "/etudiant.jsp");
	out.print("<span style='color: blue;'>You are successfully logged out!</span>");

	out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //TODO if needed
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
