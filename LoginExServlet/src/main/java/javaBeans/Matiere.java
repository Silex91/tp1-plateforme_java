package javaBeans;

public class Matiere {
	
	private static int nextIDMatiere = 0;
	
	private int IDmatiere;
	
	private String nom;
	
	private float coef;
	
	
	public Matiere(String nom, float coef) {
		this.IDmatiere = this.nextIDMatiere++;
		
		this.nom = nom;
		this.coef = coef;
	}


	/**
	 * @return the nextIDMatiere
	 */
	public static int getNextIDMatiere() {
		return nextIDMatiere;
	}


	/**
	 * @param nextIDMatiere the nextIDMatiere to set
	 */
	public static void setNextIDMatiere(int nextIDMatiere) {
		Matiere.nextIDMatiere = nextIDMatiere;
	}


	/**
	 * @return the iDmatiere
	 */
	public int getIDmatiere() {
		return IDmatiere;
	}


	/**
	 * @param iDmatiere the iDmatiere to set
	 */
	public void setIDmatiere(int iDmatiere) {
		IDmatiere = iDmatiere;
	}


	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}


	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}


	/**
	 * @return the coef
	 */
	public float getCoef() {
		return coef;
	}


	/**
	 * @param coef the coef to set
	 */
	public void setCoef(float coef) {
		this.coef = coef;
	}
	
}
