package javaBeans;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Promotion P1 = new Promotion(2020);
		
		Etudiant E1 = new Etudiant("Abdoullah");
		Etudiant E2 = new Etudiant("Alexis");
		Etudiant E3 = new Etudiant("Robert");
		Etudiant E4 = new Etudiant("Gontran");
		
		Matiere M1 = new Matiere("Biologie des particules",25);
		Matiere M2 = new Matiere("Vol de balai (dans les airs)", 10);
		Matiere M3 = new Matiere("Preparation de potions", 2);
		
		
		Note N1 = new Note(E1.getIDetu(),M1.getIDmatiere(),02);
		Note N2 = new Note(E2.getIDetu(),M1.getIDmatiere(),15);
		Note N3 = new Note(E3.getIDetu(),M1.getIDmatiere(),12);
		Note N4 = new Note(E4.getIDetu(),M1.getIDmatiere(),5);
		Note N5 = new Note(E1.getIDetu(),M2.getIDmatiere(),20);
		Note N6 = new Note(E2.getIDetu(),M2.getIDmatiere(),8);
		Note N7 = new Note(E3.getIDetu(),M2.getIDmatiere(),6);
		Note N8 = new Note(E4.getIDetu(),M2.getIDmatiere(),11);
		
		
	}

}
