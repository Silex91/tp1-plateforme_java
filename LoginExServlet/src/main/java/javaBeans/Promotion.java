package javaBeans;
import java.util.*;

public class Promotion {
	
	private int annee;
	
	private ArrayList<Matiere> listMatiere;
	
	private ArrayList<Etudiant> listEtudiants;
	
	
	public Promotion(int annee) {
		this.annee = annee;
	}
	
	public double Moyenne(Matiere mat) {
		double moyenne = 0.0;
		int cpt = 0;
		
		for(Etudiant etu : listEtudiants) {
			for(Note note : etu.getListNotes()) {
				if(note.getIDmatiere() == mat.getIDmatiere()) {
					moyenne += note.getScore();
					cpt++;
				}
			}
		}
		if(cpt > 0)
			moyenne /= cpt;
		return moyenne;
	}

	/**
	 * @return the annee
	 */
	public int getAnnee() {
		return annee;
	}

	/**
	 * @param annee the annee to set
	 */
	public void setAnnee(int annee) {
		this.annee = annee;
	}

	/**
	 * @return the listMatiere
	 */
	public ArrayList<Matiere> getListMatiere() {
		return listMatiere;
	}

	/**
	 * @param listMatiere the listMatiere to set
	 */
	public void setListMatiere(ArrayList<Matiere> listMatiere) {
		this.listMatiere = listMatiere;
	}

	/**
	 * @return the listEtudiants
	 */
	public ArrayList<Etudiant> getListEtudiants() {
		return listEtudiants;
	}

	/**
	 * @param listEtudiants the listEtudiants to set
	 */
	public void setListEtudiants(ArrayList<Etudiant> listEtudiants) {
		this.listEtudiants = listEtudiants;
	}
	
}
